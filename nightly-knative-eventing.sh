#!/bin/sh
eval $(cat vm.config)
# https://knative.dev/docs/install/any-kubernetes-cluster/
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF

kubectl apply  --selector knative.dev/crd-install=true \
--filename https://storage.googleapis.com/knative-nightly/eventing/latest/eventing.yaml

kubectl apply --filename https://storage.googleapis.com/knative-nightly/eventing/latest/eventing.yaml

# The following command installs an implementation of Channel that runs in-memory. 
# This implementation is nice because it is simple and standalone, but it is unsuitable for production use cases.

kubectl apply --filename https://storage.googleapis.com/knative-nightly/eventing/latest/in-memory-channel.yaml

# The following command installs an implementation of Broker that utilizes Channels:

kubectl apply --filename https://storage.googleapis.com/knative-nightly/eventing/latest/mt-channel-broker.yaml

kubectl apply --filename config/config-br-mt.yml


# ====== wait ... ======
kubectl wait --for=condition=available deployment/imc-dispatcher -n knative-eventing 
kubectl wait --for=condition=available deployment/imc-controller -n knative-eventing 
kubectl wait --for=condition=available deployment/eventing-webhook -n knative-eventing
kubectl wait --for=condition=available deployment/eventing-controller -n knative-eventing
kubectl wait --for=condition=available deployment/broker-controller -n knative-eventing

kubectl get pods --namespace knative-eventing
EOF





