#!/bin/sh
eval $(cat vm.config)
# https://knative.dev/docs/install/any-kubernetes-cluster/
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# Since Knative has its own network layer, we need to disable k3s' Traefik during its installation
# to make sure Kourier proxy gets a LoadBalancer IP
#curl -sfL https://get.k3s.io | sh -s - --disable traefik

# Install Knative Serving

kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-crds.yaml"
kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-core.yaml"

# Install and configure Kourier

kubectl apply --filename https://raw.githubusercontent.com/knative/serving/v${knative_version}/third_party/kourier-latest/kourier.yaml
kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress.class":"kourier.ingress.networking.knative.dev"}}'

# ====== wait ... ======
kubectl wait --for=condition=available deployment/3scale-kourier-control -n kourier-system
kubectl wait --for=condition=available deployment/3scale-kourier-gateway -n kourier-system 
kubectl wait --for=condition=available deployment/svclb-kourier -n kourier-system 

kubectl wait --for=condition=available deployment/activator -n knative-serving 
kubectl wait --for=condition=available deployment/autoscaler -n knative-serving 
kubectl wait --for=condition=available deployment/controller -n knative-serving
kubectl wait --for=condition=available deployment/webhook -n knative-serving

kubectl --namespace kourier-system get service kourier
EOF



  





