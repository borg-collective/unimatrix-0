#!/bin/sh
eval $(cat vm.config)

# Create Ubuntu VM
echo "🚧 vm creation..."

multipass launch --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
	--cloud-init ./cloud-init.yaml

# Install K3s
echo "👋 Initialize 📦 K3s on ${vm_name}..."

multipass mount config ${vm_name}:config

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | sh -s - --disable traefik
EOF

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "😃 📦 K3s initialized on ${vm_name} ✅"
echo "🖥 IP: ${IP}"

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF

cat /etc/rancher/k3s/k3s.yaml > config/k3s.yaml
sed -i "s/127.0.0.1/$IP/" config/k3s.yaml

kubectl wait --for=condition=available deployment/coredns -n kube-system 
kubectl wait --for=condition=available deployment/local-path-provisioner -n kube-system  
kubectl wait --for=condition=available deployment/metrics-server -n kube-system  

echo "🎉 K3S installation complete 🍾"
EOF
