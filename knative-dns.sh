#!/bin/sh
eval $(cat vm.config)
# https://knative.dev/docs/install/any-kubernetes-cluster/
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF

# Configure the magic xip.io DNS name
kubectl apply --filename "https://github.com/knative/serving/releases/download/v${knative_version}/serving-default-domain.yaml"

# ====== wait ... ======
kubectl wait --for=condition=available deployment/default-domain -n knative-serving

kubectl get pods --namespace knative-serving
EOF
