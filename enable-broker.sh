#!/bin/sh
eval $(cat vm.config)
# https://knative.dev/docs/install/any-kubernetes-cluster/
# https://knative.dev/development/eventing/getting-started/

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# The following command enables the default Broker on the default namespace:

kubectl label namespace default knative-eventing-injection=enabled

# Verify that the Broker is in a healthy state:
kubectl --namespace default get Broker default
EOF

# Same, but with a specific namespace:
# kubectl create namespace event-example
# kubectl label namespace event-example knative-eventing-injection=enabled
# kubectl --namespace event-example get Broker default



