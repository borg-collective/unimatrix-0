# Unimatrix 0

> WIP 🚧

## Requirements

- Multipass
- Kubectl (optional for the installation)

## Install

- Create the cluster: run `./create-vm.sh`, it creates the vm and the K3S cluster

### Knative Serving

- Install Serving component: run `./knative-serving.sh`
- Configure magic xip.io DNS name: run `./knative-dns.sh`

> - you can change the settings of the VM (before the creation) by editing `vm.config` file

### Knative Eventing

> - you don't need Eventing to use Serving

- Install Eventing component: run `./knative-eventing.sh`
- Enable default broker: run `./enable-broker.sh`

### Knative Observability (optional)

- Install Observability plugin: run `./knative-monitoring.sh`

## Install nightly components

```bash
./nightly-knative-serving.sh
./nightly-knative-dns.sh
./nightly-knative-eventing.sh
./enable-broker.sh
./nightly-knative-monitoring.sh
```


