#!/bin/sh
eval $(cat vm.config)

# https://knative.dev/docs/serving/installing-logging-metrics-traces/
# https://knative.dev/docs/install/any-kubernetes-cluster/
# https://knative.dev/docs/install/any-kubernetes-cluster/#installing-the-observability-plugin

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
kubectl apply --filename https://storage.googleapis.com/knative-nightly/serving/latest/monitoring-core.yaml
kubectl apply --filename https://storage.googleapis.com/knative-nightly/serving/latest/monitoring-metrics-prometheus.yaml
EOF





