#!/bin/sh
eval $(cat vm.config)

# https://knative.dev/docs/serving/installing-logging-metrics-traces/
# https://knative.dev/docs/install/any-kubernetes-cluster/
# https://knative.dev/docs/install/any-kubernetes-cluster/#installing-the-observability-plugin

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/monitoring-core.yaml
kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/monitoring-metrics-prometheus.yaml
EOF




