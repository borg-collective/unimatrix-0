#!/bin/sh
eval $(cat vm.config)
# https://knative.dev/docs/install/any-kubernetes-cluster/
# https://knative.dev/development/eventing/getting-started/

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# Verify that the Broker is in a healthy state:
kubectl --namespace default get Broker default
EOF



